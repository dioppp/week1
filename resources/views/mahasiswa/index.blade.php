<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Daftar Mahasiswa</title>
</head>

<body>
    <h1>Daftar Mahasiswa</h1>
    <button type="button"><a href="{{route('mahasiswa.create')}}">Tambah Data</a></button>
    <br><br>
    <table border="1">
        <tr>
            <th>No.</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>Program Studi</th>
            <th>Fakultas</th>
            <th>Jenis Kelamin</th>
        </tr>
        @forelse ($mhs as $key=>$m)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $m->nim }}</td>
                <td>{{ $m->nama }}</td>
                <td>{{ $m->prodi }}</td>
                <td>{{ $m->fakultas }}</td>
                <td>{{ $m->jenis_kelamin }}</td>
            </tr>
        @empty
            <tr>
                <td colspan="6">Belum ada data.</td>
            </tr>
        @endforelse
    </table>
</body>

</html>
