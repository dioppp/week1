<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Input Data Mahasiswa</title>
</head>

<body>
    <h1>Input Data Mahasiswa</h1>
    <form action="{{ route('mahasiswa.store') }}" method="POST">
        @csrf
        <b>NIM</b>
        <br>
        <input type="text" name="nim" size="30" placeholder="Enter your NIM here">
        <br><br>
        <b>Nama</b>
        <br>
        <input type="text" name="nama" size="30" placeholder="Enter your name here">
        <br><br>
        <b>Program Studi</b>
        <br>
        <select name="prodi" size="1">
            <option value="null" disabled selected hidden>Pilih Program Studi</option>
            <option value="Teknik Informatika">Teknik Informatika</option>
            <option value="Teknik Komputer">Teknik Komputer</option>
            <option value="Sistem Informasi">Sistem Informasi</option>
            <option value="Rekayasa Perangkat Lunak">Rekayasa Perangkat Lunak</option>
            <option value="Sistem Informasi Kelautan">Sistem Informasi Kelautan</option>
        </select>
        <br><br>
        <b>Fakultas</b>
        <br>
        <select name="fakultas" size="1">
            <option value="null" disabled selected hidden>Pilih Fakultas</option>
            <option value="Teknologi Informasi">Teknologi Informasi</option>
            <option value="Ilmu Komputer">Ilmu Komputer</option>
            <option value="Lain-lain">Lain-lain</option>
        </select>
        <br><br>
        <b>Jenis Kelamin</b>
        <br>
        <input type="radio" id='laki' name="jenis_kelamin" value="Laki-laki"><label for="laki">Laki-laki</label><br>
        <input type="radio" id='pr' name="jenis_kelamin" value="Perempuan"><label for="pr">Perempuan</label><br>
        <br><br>
        <button type="submit">Submit</button>
    </form>
</body>

</html>
